﻿using System.Data.Entity.ModelConfiguration;
using UPiloto.Models;

namespace UPiloto.Mappings
{
    public class ActividadMap : EntityTypeConfiguration<Actividad>
    {
        public ActividadMap()
        {
            
            //Agregar Llave Primaria
            HasKey(k => k.ActividadID);

            //Configurar campo titulo
            Property(p => p.Titulo)
                .HasColumnType("varchar")
                .HasMaxLength(100)
                .IsRequired();


            //Configurar campo Detalle
            Property(p => p.Detalle)
                .HasColumnType("varchar")
                .HasMaxLength(500)
                .IsRequired();

            //Agregar Llave Foranea con la tabla Solicitudes
            HasRequired(t => t.Solicitud)
                .WithMany(t => t.Actividades)
                .HasForeignKey(t => t.SolicitudID);

            //Agregar Llave Foranea con la tabla Estados
            HasRequired(t => t.EstadoActividad)
                .WithMany(t => t.Actividades)
                .HasForeignKey(t => t.EstadoActividadID);
                
            //Llave Foranea con la tabla EstadoActividad
            HasRequired(t => t.EmpleadoResponsable)
                .WithMany(t => t.Actividades)
                .HasForeignKey(t => t.EmpleadoResponsableID);


            HasOptional(c => c.ActividadPadre)
                .WithMany()
                .HasForeignKey(x => x.ActividadPadreID);

        }
    }
}