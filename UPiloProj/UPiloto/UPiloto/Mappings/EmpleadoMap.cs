﻿using System.Data.Entity.ModelConfiguration;
using UPiloto.Models;

namespace UPiloto.Mappings
{
    public class EmpleadoMap : EntityTypeConfiguration<Empleado>
    {
        public EmpleadoMap()
        {
            //Agregar Llave Primaria
            HasKey(k => k.EmpleadoID);

            //Agregar Llave Foranea
            HasRequired(t => t.Cargo)
                .WithMany(s => s.Empleados)
                .HasForeignKey(s => s.CargoID);

            //Agregar Llave Foranea
            HasRequired(t => t.Rol)
                .WithMany(s => s.Empleados)
                .HasForeignKey(s => s.RolID);
        }
    }
}