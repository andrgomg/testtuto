﻿using System.Data.Entity.ModelConfiguration;
using UPiloto.Models;

namespace UPiloto.Mappings
{
    public class SolicictudMap : EntityTypeConfiguration<Solicitud>
    {
        public SolicictudMap()
        {
            //Agregar Llave Primaria
            HasKey(k => k.SolicitudID);

            //Configurar campo titulo
            Property(p => p.Titulo)
                .HasColumnType("varchar")
                .HasMaxLength(100)
                .IsRequired();


            //Configurar campo Detalle
            Property(p => p.Detalle)
                .HasColumnType("varchar")
                .HasMaxLength(500)
                .IsRequired();

        }
    }
}