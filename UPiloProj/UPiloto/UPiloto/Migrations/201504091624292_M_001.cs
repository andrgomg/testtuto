namespace UPiloto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empleado",
                c => new
                    {
                        EmpleadoID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 40),
                        Apellido = c.String(maxLength: 40),
                        EmailAddress = c.String(),
                        Phone = c.String(maxLength: 25),
                        CargoID = c.Int(nullable: false),
                        RolID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmpleadoID)
                .ForeignKey("dbo.Cargo", t => t.CargoID, cascadeDelete: true)
                .ForeignKey("dbo.Rol", t => t.RolID, cascadeDelete: true)
                .Index(t => t.CargoID)
                .Index(t => t.RolID);
            
            CreateTable(
                "dbo.Actividad",
                c => new
                    {
                        ActividadID = c.Int(nullable: false, identity: true),
                        ActividadPadreID = c.Int(nullable: false),
                        Titulo = c.String(),
                        Detalle = c.String(),
                        NumeroOrdenLogico = c.Int(nullable: false),
                        Nivel = c.Short(nullable: false),
                        FechaCreacion = c.DateTime(nullable: false),
                        FechaFinalizacionEsperada = c.DateTime(nullable: false),
                        Terminada = c.Boolean(nullable: false),
                        SolicitudID = c.Int(nullable: false),
                        EmpleadoResponsableID = c.Int(nullable: false),
                        EstadoActividadID = c.Short(nullable: false),
                        ActividadPadre_ActividadID = c.Int(),
                        EmpleadoResponsable_EmpleadoID = c.Int(),
                    })
                .PrimaryKey(t => t.ActividadID)
                .ForeignKey("dbo.Actividad", t => t.ActividadPadre_ActividadID)
                .ForeignKey("dbo.Empleado", t => t.EmpleadoResponsable_EmpleadoID)
                .ForeignKey("dbo.EstadoActividad", t => t.EstadoActividadID, cascadeDelete: true)
                .ForeignKey("dbo.Solicitud", t => t.SolicitudID, cascadeDelete: true)
                .Index(t => new { t.ActividadPadreID, t.NumeroOrdenLogico }, unique: true, name: "IX_Actividad_ArbolDependencias")
                .Index(t => t.SolicitudID)
                .Index(t => t.EstadoActividadID)
                .Index(t => t.ActividadPadre_ActividadID)
                .Index(t => t.EmpleadoResponsable_EmpleadoID);
            
            CreateTable(
                "dbo.EstadoActividad",
                c => new
                    {
                        EstadoActividadID = c.Short(nullable: false, identity: true),
                        Descripcion = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.EstadoActividadID);
            
            CreateTable(
                "dbo.Solicitud",
                c => new
                    {
                        SolicitudID = c.Int(nullable: false, identity: true),
                        Titulo = c.String(),
                        Detalle = c.String(),
                        FechaCreacion = c.DateTime(nullable: false),
                        EmpleadoCreaID = c.Int(nullable: false),
                        Empleado_EmpleadoID = c.Int(),
                    })
                .PrimaryKey(t => t.SolicitudID)
                .ForeignKey("dbo.Empleado", t => t.Empleado_EmpleadoID)
                .Index(t => t.Empleado_EmpleadoID);
            
            CreateTable(
                "dbo.Cargo",
                c => new
                    {
                        CargoID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.CargoID);
            
            CreateTable(
                "dbo.Rol",
                c => new
                    {
                        RolID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.RolID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Empleado", "RolID", "dbo.Rol");
            DropForeignKey("dbo.Empleado", "CargoID", "dbo.Cargo");
            DropForeignKey("dbo.Solicitud", "Empleado_EmpleadoID", "dbo.Empleado");
            DropForeignKey("dbo.Actividad", "SolicitudID", "dbo.Solicitud");
            DropForeignKey("dbo.Actividad", "EstadoActividadID", "dbo.EstadoActividad");
            DropForeignKey("dbo.Actividad", "EmpleadoResponsable_EmpleadoID", "dbo.Empleado");
            DropForeignKey("dbo.Actividad", "ActividadPadre_ActividadID", "dbo.Actividad");
            DropIndex("dbo.Solicitud", new[] { "Empleado_EmpleadoID" });
            DropIndex("dbo.Actividad", new[] { "EmpleadoResponsable_EmpleadoID" });
            DropIndex("dbo.Actividad", new[] { "ActividadPadre_ActividadID" });
            DropIndex("dbo.Actividad", new[] { "EstadoActividadID" });
            DropIndex("dbo.Actividad", new[] { "SolicitudID" });
            DropIndex("dbo.Actividad", "IX_Actividad_ArbolDependencias");
            DropIndex("dbo.Empleado", new[] { "RolID" });
            DropIndex("dbo.Empleado", new[] { "CargoID" });
            DropTable("dbo.Rol");
            DropTable("dbo.Cargo");
            DropTable("dbo.Solicitud");
            DropTable("dbo.EstadoActividad");
            DropTable("dbo.Actividad");
            DropTable("dbo.Empleado");
        }
    }
}
