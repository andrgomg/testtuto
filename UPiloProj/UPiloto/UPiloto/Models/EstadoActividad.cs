﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UPiloto.Models
{
    public partial class EstadoActividad
    {

        /*
         Estados
         * Sin Aprobar 
         * Aprobada
         * Iniciada
         * Finalizada
         * Revisada
         */


        public Int16 EstadoActividadID { get; set; }
        
        [StringLength(250)]
        public string Descripcion { get; set; }


        public virtual ICollection<Actividad> Actividades{ get; set; }


    }
}