﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UPiloto.Models
{
    public partial class Empleado 

    {
        public int EmpleadoID { get; set; }
        
        [StringLength(40)]
        public string Nombre { get; set; }

        [StringLength(40)]
        public string Apellido { get; set; }

        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [StringLength(25)]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        public int CargoID { get; set; }

        public int RolID { get; set; }

        public virtual Cargo Cargo { get; set; }
        public virtual Rol Rol { get; set; }

        public virtual ICollection<Actividad> Actividades { get; set; }
    }
}