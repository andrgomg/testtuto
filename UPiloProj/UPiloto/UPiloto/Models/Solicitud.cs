﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UPiloto.Models
{
    public partial class Solicitud

    {
        public int SolicitudID { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int EmpleadoCreaID { get; set; }
        public virtual Empleado Empleado { get; set; }

        public virtual ICollection<Actividad> Actividades { get; set; }
        
    }
}
