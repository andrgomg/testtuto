﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UPiloto.Models
{
    public partial class Cargo
    {
        /// <summary>
        /// ID del cargo del empleado
        /// </summary>
        public int CargoID { get; set; }
        
        [StringLength(250)]
        public string Descripcion { get; set; }


        public virtual ICollection<Empleado> Empleados { get; set; }

    }
}