﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;


namespace UPiloto.Models
{
    public class Actividad

    {
        public int ActividadID { get; set; }

        [Index("IX_Actividad_ArbolDependencias" ,1,IsUnique = true )]
        public int ActividadPadreID { get; set; }
        public string Titulo { get; set; }
        public string Detalle { get; set; }
        /// <summary>
        /// Define la secuencia de orden de ejecucion de las actividades dentro de su respectivo nivel
        /// </summary>
        [Index("IX_Actividad_ArbolDependencias" ,2,IsUnique = true )]
        public Int32 NumeroOrdenLogico { get; set; }
        /// <summary>
        /// Define el nivel de profuncidad de la acividad que se va a desarrollar
        /// Las tareas de primer nivel son generales , las de segundo nivel son dependientes de las de primer nivel
        /// </summary>
        public Int16 Nivel{ get; set; }

        public DateTime FechaCreacion { get; set; }
        public DateTime FechaFinalizacionEsperada { get; set; }

        [DefaultValue(true)]
        public bool Terminada { get; set; }

        public int SolicitudID { get; set; }

        public int EmpleadoResponsableID { get; set; }

        public Int16 EstadoActividadID { get; set; }


        //Foreing Key References
        public virtual Solicitud Solicitud{ get; set; }
        public virtual EstadoActividad EstadoActividad { get; set; }
        public virtual Empleado EmpleadoResponsable { get; set; }
        public virtual Actividad ActividadPadre { get; set; }
        
    }
}
