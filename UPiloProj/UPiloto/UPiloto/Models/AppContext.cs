﻿namespace UPiloto.Models
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;
    
    public class AppContext : DbContext
    {
        
        public AppContext(): base("name=AppModel")
        {

        }

       public virtual DbSet<Empleado> Empleados { get; set; }
       public virtual DbSet<Solicitud> Solicitudes { get; set; }


       protected override void OnModelCreating(DbModelBuilder modelBuilder)
       {
           // Configure Code First to ignore PluralizingTableName convention 
           // If you keep this convention, the generated tables  
           // will have pluralized names. 
           modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

           //Configurar las llaves primarias
           modelBuilder.Entity<Cargo>().HasKey(k => k.CargoID);
           
           modelBuilder.Entity<Rol>().HasKey(k => k.RolID);

           

       } 
    }

   
}