﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UPiloto.Models
{
    public partial class Rol
    {
        public int RolID { get; set; }
        
        [StringLength(250)]
        public string Descripcion { get; set; }

        public virtual ICollection<Empleado> Empleados { get; set; }

    }
}